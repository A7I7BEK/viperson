

/*__________ Registration and Sign-in +++ __________*/
$('.reg_item.sign_in, .mod_reg_had_accout > a').click(function (e) {
	$('.mod_sign').addClass('active');
	$('.mod_register').removeClass('active');

	e.preventDefault();
});

$('.reg_item.register, .mod_sign_no_accout > a').click(function (e) {
	$('.mod_register').addClass('active');
	$('.mod_sign').removeClass('active');

	e.preventDefault();
});
/*__________ Registration and Sign-in --- __________*/





/*__________ Validation +++ __________*/
$('.mod_reg_submit > button').click(function () {
	var emailStatus;

	var email = $('.mod_reg_txt_in.email').val();
	var errorEmail = $('.mod_reg_error.email');


	// Count '@' sign
	var countSign = 0;
	var signIndex = 0;
	for (var i = 0; i < email.length; i++) {
		if (email[i] == '@')
		{
			countSign++;
			signIndex = i;
		}
	}

	// Check for absence of Domain Name 
	var checkNoDomain = true;
	for (var i = 0; i < email.substr(signIndex + 1).length; i++) {
		if (email.substr(signIndex + 1)[i] == '.')
		{
			checkNoDomain = false;
			break;
		}
	}


	if (email == '')
	{
		emailStatus = ErrorMessage(errorEmail, 'Пожалуйста, заполните поля электронной почты');
	}
	else if (countSign != 1)
	{
		emailStatus = ErrorMessage(errorEmail, 'Электронная почта должна содержать один знак \"@\"');
	}
	else if (checkNoDomain)
	{
		emailStatus = ErrorMessage(errorEmail, 'Пожалуйста, укажите имя домена');
	}
	else
	{
		errorEmail.addClass('hidden');
		emailStatus = true;
	}

	if (emailStatus)
	{
		return true;
	}
	else
	{
		return false;
	}
});

$('.mod_sign_submit > button').click(function () {
	var name = $('.mod_sign_txt_in.name').val();
	var password = $('.mod_sign_txt_in.password').val();

	var errorSignIn = $('.mod_sign_error.name_pass');

	if (name == 'Azizbek' & password == '123')
	{
		errorSignIn.addClass('hidden');
		return true;
	}
	else
	{
		return ErrorMessage(errorSignIn, 'Логин или пароль неправильный!');
	}
});

function ErrorMessage(element, text) {
	element.text(text);
	element.removeClass('hidden');
	return false;
}
/*__________ Validation --- __________*/




/*__________ Mobile Menu +++ __________*/
$('.nav_menu_btn, .nav_menu_bg').click(function () {
	if ($('.nav_menu_btn').hasClass('active'))
	{
		$('.nav_menu_btn').removeClass('active');
		$('.nav_menu_box').removeClass('active');
		$('.nav_menu_bg').removeClass('active');

		$('html, body').removeClass('over_hidden');
		$('body').attr('style', '');
	}
	else {
		$('.nav_menu_btn').addClass('active');
		$('.nav_menu_box').addClass('active');
		$('.nav_menu_bg').addClass('active');

		$('html, body').addClass('over_hidden');
		$('body').attr('style', 'padding-right: 15px');
	}
});
/*__________ Mobile Menu --- __________*/




/*__________ Catalog Item +++ __________*/
$('.catalog_menu > li:not(.more) > a').click(function () {

	var i = $(this).parent().index();
	$(this).closest('.catalog').find('.catalog_item').eq(i).addClass('active').siblings().removeClass('active');

	$(this).parent().addClass('active').siblings().removeClass('active');

	return false;
});
/*__________ Catalog Item --- __________*/





/*__________ Question +++ __________*/
$('.quest_btn > p').click(function () {
	$(this).toggleClass('active');
	$(this).parent().nextAll('.quest_text_box').toggleClass('active');
});
/*__________ Question --- __________*/





/*__________ Leave comment +++ __________*/
$('.leave_comment_text > textarea').focus(function () {
	$('.leave_comment').addClass('active');
});

$(document).click(function(e) {
	if ($(e.target).is('.leave_comment_text > textarea') === false) {
		$('.leave_comment').removeClass('active');
	}
});
/*__________ Leave comment --- __________*/




/*__________ Service Description +++ __________*/
$('.service_desc_item').click(function () {

	var i = $(this).index();
	$(this).addClass('active').siblings().removeClass('active');

	$(this).closest('.service_desc').find('.service_desc_text').eq(i).addClass('active').siblings().removeClass('active');
});
/*__________ Service Description --- __________*/




/*__________ Be Partner +++ __________*/
$('.baner_partner_btn > a').click(function () {
	var scrollFromTop = $('.contact_partner').offset().top;

	$('html').animate({ scrollTop: scrollFromTop }, 700);

	return false;
});
/*__________ Be Partner --- __________*/





/*__________ Nav Sign +++ __________*/
$('.nav_sign').click(function () {
	$(this).toggleClass('active');
	$(this).find('.nav_sign_list_box').toggleClass('active');
});

$(document).click(function(e) {
	if ($(e.target).is('.nav_sign, .nav_sign *') === false) {
		$('.nav_sign').removeClass('active');
		$('.nav_sign_list_box').removeClass('active');
	}
});
/*__________ Nav Sign --- __________*/





/*__________ PC Settings +++ __________*/
$('.pc_settings_btn_edit').click(function () {
	$(this).addClass('hidden');
	$('.pc_settings_btn_save').removeClass('hidden');
	
	$('.pc_settings_in').removeAttr('disabled');
});

$('.pc_settings_btn_save').click(function () {
	$(this).addClass('hidden');
	$('.pc_settings_btn_edit').removeClass('hidden');

	$('.pc_settings_in').attr('disabled', '');
});



$('.pass_btn_change').click(function () {
	$(this).addClass('hidden');
	$('.pass_btn_save').removeClass('hidden');
	
	$('.pc_settings_pass').removeAttr('disabled');
});

$('.pass_btn_save').click(function () {
	$(this).addClass('hidden');
	$('.pass_btn_change').removeClass('hidden');

	$('.pc_settings_pass').attr('disabled', '');
});
/*__________ PC Settings --- __________*/






/*__________ Mobile --- __________*/







